# Ready for Genshin v2.6.0

## Requirements

- `Wine` version in range [5.3, 7.0);
- `freetype2` version 2.10.4 to prevent an [issue](#freetype2-issue);
- Vulkan-capable graphics card;
- 40+ Gb of free space.

## Preparations

1. You must install a supported graphics driver and Vulkan dependencies, [guide here](https://github.com/lutris/docs/blob/master/InstallingDrivers.md). Also, for additional information Wiki [Lutris](https://github.com/lutris/lutris/wiki) and [DXVK](https://github.com/doitsujin/dxvk/wiki).
1. Install [Wine-staging](https://github.com/lutris/docs/blob/master/WineDependencies.md) and its dependencies.
1. Install [Lutris](https://lutris.net/downloads/) app.
1. Install are [`git`](https://git-scm.com/download/linux), [`python-magic`](https://pypi.org/project/python-magic/), [`zenity`](https://help.gnome.org/users/zenity/stable/) and [`xdelta3`](http://xdelta.org/) packages (Arch-based: `sudo pacman -S git python-magic zenity xdelta3`).

## Guide to using the GI-installer

1. Open terminal
1. Download this repo: `$ git clone https://gitlab.com/hikkidev/gi-lutris.git`
1. Change folder: `$ cd gi-lutris`
1. Run Lutris: `$ lutris -di $(pwd)/lutris-gi-installation-script.yml`

> Recommendation: run Lutris in debug mode (-d) to see logs and errors

Single line exec for copy-paste:

```cmd
git clone https://gitlab.com/hikkidev/gi-lutris.git && cd gi-lutris && lutris -di $(pwd)/lutris-gi-installation-script.yml
```

## Game installation guide

1. Download and install Genshin via official launcher (`!with DEFAULT path!`)
1. Once it is done, **DO NOT** press launch button. Just **CLOSE** the launcher (choice: `Exit program`).
1. In Lutris: Open Winetricks via the context menu:

![winetricks](/images/open-winetricks.png)

4. Then select the default wineprefix and click `OK`:

![winetricks](/images/choose-default-prefix.png)

5. Install the Windows component `vcrun2019` and the font `corefonts`.

![winetricks](/images/choose-installation.png)

![winetricks](/images/vcrun2019.png)
![winetricks](/images/corefonts.png)

6. In Lutris:  Right click on the game, In context menu → `Configure` → `Game options` → **Executable**.
1. Browse and select `launcher.bat` which you can find in the same directory as `GenshinImpact.exe`
<br>Sample: `(your wineprefix)/drive_c/Program Files/Genshin Impact/Genshin Impact game/launcher.bat`
1. Game is ready.

### Post-installation

Clean up `$ cd .. && rm -R gi-lutris/`

Recomended runner: `lutris-6.14-4-x86_64`

### Package [`xdelta3`](http://xdelta.org/)

- Debian/Ubuntu: `apt install xdelta3`
- Fedora: `dnf install xdelta`
- Arch-based: `pacman -S xdelta3`
- macOS: `port install xdelta` or `brew install xdelta`

### freetype2 issue

```cmd
 /usr/lib/libfreetype.so.6: undefined symbol: hb_ot_tags_from_script_and_language
```

1. Check `freetype2` version (`sudo pacman -Q freetype2`)
1. If version 2.11 or higher, downgrade them to 2.10.4 (`sudo pacman -U https://archive.archlinux.org/packages/path/freetype2-2.10.4-1-x86_64.pkg.tar.zst`)
